# beginning of create_db.py
import json
#from models import app, db, Book

def load_json(filename):
    with open(filename) as file:
        jsn = json.load(file)
        file.close()

    return jsn

book = load_json('books.json')
print(book)